﻿using System;
using System.Collections.Generic;
using System.Text;
using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Table;
using Stimulsoft.Report.Dictionary;
using System.Data;
using System.Drawing;
namespace StimulsoftApp
{
    public class Report
    {
        public StiReport StiReport { get; private set; }
        public Report(object list)
        {
             StiReport = new StiReport();
            StiReport.ScriptLanguage = StiReportLanguageType.CSharp;
            StiReport.RegData("MyList", list);
            StiReport.Dictionary.Synchronize();

            StiPage page = StiReport.Pages.Items[0];
            StiHeaderBand headerBand = new StiHeaderBand();
            headerBand.Name = "HeaderBand";
            page.Components.Add(headerBand);
            StiDataBand dataBand = new StiDataBand();
            dataBand.DataSourceName = "MyList";
            dataBand.Height = 0.5f;
            dataBand.Name = "DataBand";
            page.Components.Add(dataBand);

            StiDataSource dataSource = StiReport.Dictionary.DataSources[0];

            Double pos = 0;
            Double columnWidth = StiAlignValue.AlignToMinGrid(page.Width / dataSource.Columns.Count, 0.1, true);
            int nameIndex = 1;
            foreach (StiDataColumn column in dataSource.Columns)
            {
                if (column.Name == "_ID" || column.Name == "_Current") continue;

                //Create text on header
                StiText headerText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                headerText.Text.Value = column.Name;
                headerText.HorAlignment = StiTextHorAlignment.Center;
                headerText.Name = "HeaderText" + nameIndex.ToString();
                headerText.Brush = new StiSolidBrush(Color.MediumSeaGreen);
                headerText.Border.Side = StiBorderSides.All;
                headerBand.Components.Add(headerText);

                //Create text on Data Band
                StiText dataText = new StiText(new RectangleD(pos, 0, columnWidth, 0.5f));
                dataText.Text.Value = "{MyList." + column.Name + "}";
                dataText.Name = "DataText" + nameIndex.ToString();
                dataText.Border.Side = StiBorderSides.All;

                dataBand.Components.Add(dataText);

                pos += columnWidth;

                nameIndex++;
            }

            //Create FooterBand
            StiFooterBand footerBand = new StiFooterBand();
            footerBand.Height = 0.5f;
            footerBand.Name = "FooterBand";
            page.Components.Add(footerBand);

            //Create text on footer
            StiText footerText = new StiText(new RectangleD(0, 0, page.Width, 0.5f));
            footerText.Text.Value = "Count - {Count()}";
            footerText.HorAlignment = StiTextHorAlignment.Right;
            footerText.Name = "FooterText";
            footerText.Brush = new StiSolidBrush(Color.LightGreen);
            footerBand.Components.Add(footerText);

            StiReport.Render(false);

            //     report.Show();

         }
        public void ExportDocumentPdf(string fileName)
        {
            StiReport.ExportDocument(StiExportFormat.Pdf, $"{fileName}.pdf");
        }
        public void ExportDocumentCsv(string fileName)
        {
            StiReport.ExportDocument(StiExportFormat.Csv, $"{fileName}.csv");
        }
    }
}
