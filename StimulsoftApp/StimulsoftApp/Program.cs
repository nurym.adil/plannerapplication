﻿using Stimulsoft.Base;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Components.Table;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.Caching;

namespace StimulsoftApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Program program = new Program();
            //program.SendReport("Nurym.adil@gmail.com");
            //program.SendReport("Nurym.adil@gmail.com");
        }
        public void SendReport(string email)
        {
            Report report = new Report(new List<string>
            {
                DateTime.Now.ToString(),
                DateTime.Now.ToString(),
                DateTime.Now.ToString()
            });
            string fileNameCsv = $@"{Guid.NewGuid()}";
            string fileNamePdf = $@"{Guid.NewGuid()}";
            report.ExportDocumentCsv(fileNameCsv);
            report.ExportDocumentPdf(fileNamePdf);
            Message message = new Message();
            message.SendMessage(email, "Report", fileNameCsv + ".csv", fileNamePdf + ".pdf");
        }
    }
}
