﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlannerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Timer timer;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SaveButton(object sender, RoutedEventArgs e)
        {
            if (emailTextBox.Text != string.Empty && periodTextBox.Text != string.Empty)
            {
                double.TryParse(periodTextBox.Text, out double result);
                timer = new Timer(new TimerCallback(SendReport), emailTextBox.Text, TimeSpan.Zero, TimeSpan.FromSeconds(result));
            }
            else
            {
                MessageBox.Show("Заполните все поля");
            }
        }

        private void SendReport(object email)
        {
            var context = new CollectibleAssemblyLoadContext();
            var assemblyPath = @"C:\Users\nurym\source\repos\PlannerApplication\StimulsoftApp\StimulsoftApp\bin\Debug\netcoreapp3.1\StimulsoftApp.dll";
            WeakReference weakReference;

            using (var fs = new FileStream(assemblyPath, FileMode.Open, FileAccess.Read))
            {
                var assembly = context.LoadFromStream(fs);

                var type = assembly.GetType("StimulsoftApp.Program");
                var greetMethod = type.GetMethod("SendReport");

                var instance = Activator.CreateInstance(type);
                greetMethod.Invoke(instance, new object[] { email as string });
                weakReference = new WeakReference(context, true);

            }
            context.Unload();


            for (int i = 0; weakReference.IsAlive && (i < 100); i++)
            {

                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
